$( document ).ready( function() {
	$( 'img' ).retinizeImages();
	$( '.parallax' ).snowBridge({
		fullscreen: false,
		scaleContainer: true,
		scaleUnder: 960,
		fadeInOut: false
	});
});
